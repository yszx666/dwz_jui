const path = require('path');
module.exports = {
  //其他参数
  mode: 'production',
  entry:[
    "./js/dwz.core.js",
    "./js/dwz.util.date.js",
    "./js/dwz.validate.method.js",
    "./js/dwz.barDrag.js",
    "./js/dwz.drag.js",
    "./js/dwz.tree.js",
    "./js/dwz.ui.js",
    "./js/dwz.theme.js",
    "./js/dwz.switchEnv.js",
    "./js/dwz.alertMsg.js",
    "./js/dwz.contextmenu.js",
    "./js/dwz.navTab.js",
    "./js/dwz.tab.js",
    "./js/dwz.resize.js",
    "./js/dwz.dialog.js",
    "./js/dwz.dialogDrag.js",
    "./js/dwz.sortDrag.js",
    "./js/dwz.cssTable.js",
    "./js/dwz.stable.js",
    "./js/dwz.taskBar.js",
    "./js/dwz.ajax.js",
    "./js/dwz.pagination.js",
    "./js/dwz.database.js",
    "./js/dwz.datepicker.js",
    "./js/dwz.effects.js",
    "./js/dwz.panel.js",
    "./js/dwz.checkbox.js",
    "./js/dwz.combox.js",
    "./js/dwz.file.js",
    "./js/dwz.history.js",
    "./js/dwz.print.js"],
  output: {
    filename: 'dwz.min.js',
    path: path.resolve(__dirname, 'dist')
  }
};